#
# SYNOPSIS
#
#   TSU_GROUPED_TARGETS([run-if-true],[run-if-false])
#
# DESCRIPTION
#
#   Check whether Make supports invoking a recipe only once to build
#   multiple targets with the "grouped targets" feature and "&:" syntax.
#   This was a feature introduced in GNU Make 4.3, but we are testing
#   the feature itself rather than the Make version.
#
#   If Make supports grouped targets, then the run-if-true code is
#   executed.  Otherwise, the run-if-false code is executed.
#
#   The substitution variable "grouped" is set to "&" if Make supports
#   grouped targets and to the empty string if not; this allows use of
#   the syntax "@grouped@:" to write recipes that will be grouped if
#   possible and not alarming to Make when the feature doesn't exist.
#
# LICENSE
#
#   This macro is released to the public domain by its author,
#   Matthew Skala <mskala@ansuz.sooke.bc.ca>.

#serial 1

AC_DEFUN([TSU_GROUPED_TARGETS], [
  AC_REQUIRE([AC_PROG_GREP])
  AC_REQUIRE([AC_PROG_MAKE_SET])
  AC_MSG_CHECKING([whether Make supports grouped targets])
  : "${TMPDIR:=/tmp}"
  {
    dir=`(umask 077 && mktemp -d "$TMPDIR/fooXXXXXX") 2>/dev/null` &&
    test -d "$dir"
  } || {
    dir=$TMPDIR/foo$$-$RANDOM
    (umask 077 && mkdir "$dir")
  }
  echo 'foo bar &: Makefile' > $dir/Makefile
  echo '	@echo YEAH' >> $dir/Makefile
  _make_gt_result=`$am_make -C $dir foo bar | $GREP YEAH`
  rm -rf $dir
  AC_SUBST([grouped],[])
  AS_IF([test "x$_make_gt_result" = "xYEAH"],
    [AC_MSG_RESULT([yes])
     grouped='&'
     $1],
    [AC_MSG_RESULT([no])
     grouped=''
     $2])
])
